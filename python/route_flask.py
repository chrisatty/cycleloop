from flask import Flask, request, url_for, send_from_directory
from flask.json import jsonify

from route import get_circle

from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)

@app.route('/getpath/', methods=['GET', 'POST'])
@cross_origin()
def do_route():
    if 'lat' in request.values and 'lng' in request.values and 'plen' in request.values:
        lat = float(request.values['lat'])
        lng = float(request.values['lng'])
        plen = float(request.values['plen'])
        points, text = get_circle(lat,lng,plen)
        return jsonify({'path':points, 'text':text})

if __name__ == "__main__":
    app.run(host= '0.0.0.0')
