import numpy as np
from elasticsearch import Elasticsearch

# Map internal feature names to human-friendly labels
FEATURE_FRIENDLY_NAME = {
    'arts_facility': 'Arts Facility',
    'barbeque': 'Barbecue',
    'dog_park': 'Dog Park',
    'drinking_fountain': 'Drinking Fountain',
    'fitness_site': 'Fitness Site',
    'graffiti_site': 'Graffiti Site',
    'public_art': 'Public Art',
    'public_furniture': 'Public Furniture',
    'speed_camera': 'Speed Camera',
    'speed_fine': 'Speed Fine',
    'toilet': 'Toilet'
}

# Assign relative weight to each feature type
FEATURE_TYPE_WEIGHT = {
    'arts_facility': 1.0,
    'barbeque': 1.0,
    'dog_park': 1.0,
    'drinking_fountain': 1.0,
    'fitness_site': 1.0,
    'graffiti_site': 1.0,
    'public_art': 1.0,
    'public_furniture': 0.2,
    'speed_camera': 1.0,
    'speed_fine': 1.0,
    'toilet': 1.2,
}

ELASTIC_SEARCH_HOST = 'http://{username}:{password}@govhack.co'
es = Elasticsearch([ELASTIC_SEARCH_HOST.format(username='elastic', password='changeme')])

def get_nearby_features(latitude, longitude, distance, threshold=1000):
    assert isinstance(latitude, float), 'Latitude must be a floating point number'
    assert isinstance(longitude, float), 'Longitude must be a floating point number'
    assert isinstance(distance, (int, float)), 'Distance must be a number'

    search_params = \
{
    "size": threshold,
    "query":
    {
        "bool" :
        {
            "must" :
            {
                "match_all" : {}
            },
            "filter" :
            {
                "geo_distance" :
                {
                    "distance": '{0}m'.format(distance),
                    "lat_long" :
                    {
                        "lat" : latitude,
                        "lon" : longitude
                    }
                }
            }
        }
    }
}
    # execute Elastic Search query based on search parameters
    query_results = es.search(index='geo', body=search_params)
    feature_latlngs = []
    feature_weights = []
    feature_labels = []
    for result in query_results['hits']['hits']:
        # append lat/lon pair to list
        feature_location = result['_source']['lat_long'].split(',')
        feature_latlngs.append(feature_location)
        # append feature weight and friendly name to lists
        feature_type = result['_source']['type']
        feature_weights.append(FEATURE_TYPE_WEIGHT[feature_type])
        feature_labels.append(FEATURE_FRIENDLY_NAME[feature_type])
    # convert to numpy arrays and return
    return np.array(feature_latlngs), np.array(feature_weights), feature_labels

def main():
    locations, weights, labels = get_nearby_features(-35.0, 149.0, 200000)
    print('Query returned {0} results'.format(len(locations)))
    for i in range(0, min(10, len(locations))):
        print('result {0} has lat/lon {1}, weight {2} and label {3}'.format(i+ 1, locations[i], weights[i], labels[i]))

if __name__ == '__main__':
    main()
