import pandas as pd
from shapely.geometry import Point, MultiPoint

import numpy as np
import pyproj
import gpxpy
import gpxpy.gpx
from tsp_solver.greedy_numpy import solve_tsp
from find_points import get_nearby_features
#import tsp

num_rand_points = 1
num_int_points = 16
min_points = 15

def load_points():
    # read toilets
    data = pd.read_csv("./data/Public_Toilets_in_the_ACT.csv")
    latlng = data[["LATITUDE", "LONGITUDE"]].as_matrix()
    weights = np.ones((latlng.shape[0],))
    text = ["Toilet"] * latlng.shape[0]

    data = pd.read_csv("./data/Drinking_Fountains.csv")
    new_latlng = data[["LATITUDE", "LONGITUDE"]].as_matrix()
    new_weights = np.ones((new_latlng.shape[0],))*0.9
    new_text = ["Drink"] * new_latlng.shape[0]
    latlng = np.vstack([latlng, new_latlng])
    weights = np.concatenate([weights, new_weights])
    text.extend(new_text)
    return latlng, weights, text

def project_points(pt, inverse=False):
    pr = pyproj.Proj(init="epsg:28355")
    new_points = []
    for p in pt:
        if inverse:
            new_points.append(pr(p[0], p[1], inverse=inverse))
        else:
            new_points.append(pr(p[1], p[0], inverse=inverse))
    if inverse:
        return np.array(new_points)[:,::-1]
    else:
        return np.array(new_points)


def find_close_points_idx(me, points, max_dist = 10000, l2=False):
    if l2:
        #l2 distance
        dists = np.linalg.norm(points - me, axis=1)
    else:
        #l1 distance
        dists = np.sum(points - me, axis=1)

    close_points = dists < max_dist

    return np.flatnonzero(close_points)

def filter_points(me, points, weights, max_dist=10000, max_points=20):
    close_idx = find_close_points_idx(me, points, max_dist = max_dist, l2 = False)
    if close_idx.shape[0] > max_points:
        p = np.array(weights[close_idx])
        p /= p.sum(p)
        chosen = np.random.choice(close_idx, size = max_points, replace=False, p=p)
    return chosen

def route_points(points):
    ch = MultiPoint(data[close_points]).convex_hull
    ch.exterior.coords
    new_points = []
    for p in points:
        new_points.append(p)
    return new_points

def pol2cart(r, theta):
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    return (x, y)

def find_close_to_circle(center, radius, points):
    dists = np.abs(np.linalg.norm(points - center, axis=1) - radius)
    return np.argsort(dists)[:num_int_points]

def find_close_to_circle_weighted(center, radius, points, w):
    dists = np.abs(np.linalg.norm(points - center, axis=1) - radius)
    dists /= w
    p = 1.0/dists**2.0

    #p = np.amax(p) - p
    p = p
    allowed = dists < radius / 1.5
    if not np.any(allowed):
        c = np.argmin(dists)
        allowed[c] = 1
    #chosen = np.random.choice(np.arange(dists.shape[0])[allowed], p=p, replace=False, size=)

    # choose random points
    chosen = []
    num_to_choose = min(num_int_points, np.count_nonzero(allowed))
    for i in xrange(num_int_points):
        if not np.any(allowed):
            break
        for j in xrange(10):
            if not np.any(allowed):
                break
            p_cur = p[allowed]
            p_cur /= np.sum(p_cur)
            cchosen = np.random.choice(np.arange(dists.shape[0])[allowed], p=p_cur, replace=False)
            if chosen:
                cdist = np.amin(np.linalg.norm(points[np.array(chosen)] - points[cchosen], axis=1))
                if cdist < 2.0*np.pi*radius/20.0:
                    allowed[cchosen] = 0
                    continue
            if cchosen not in chosen:
                break
        if chosen:
            cdist = np.amin(np.linalg.norm(points[np.array(chosen)] - points[cchosen], axis=1))
        else:
            cdist = 2.0 * np.pi * radius
        if cchosen not in chosen and cdist >= 2.0 * np.pi*radius/20.0:
            allowed[cchosen] = 0
            chosen.append(cchosen)

    return np.array(chosen)#np.argsort(dists)[:num_int_points]

def order_points_tsp(points):
    dist = np.zeros((len(points)-1,len(points)-1))
    for i,p in enumerate(points[:-1]):
        for j,p2 in enumerate(points[:-1]):
            dist[i][j] = np.linalg.norm(p - p2)
    order = solve_tsp(dist)
    while order[0] != 0:
        order = np.roll(order,1)
    order = np.concatenate([order, [0]])
    return points[np.array(order)], order

def order_points(points):
    new_order = [0]
    new_points = [points[0]]
    used = np.zeros((points.shape[0],))
    for i in xrange(len(points[1:-1])):
        dists = []
        for j,test in enumerate(points[1:-1]):
            if used[j]:
                dists.append(np.inf)
                continue
            #print new_points[-1], test
            dists.append(np.linalg.norm(new_points[-1] - test))
        chosen = np.argmin(dists)+1
        new_order.append(chosen)
        new_points.append(points[chosen])
        used[chosen-1] = True
    new_points.append(points[-1])
    new_order.append(len(points)-1)
    #print used
    return np.array(new_points), np.array(new_order)

def order_points2(points, center):
    new_points = [points[0]]
    new_order = [0]
    used = np.zeros((points.shape[0],))
    for i in xrange(len(points[1:-1])):
        dists = []
        for j,test in enumerate(points[1:-1]):
            if used[j]:
                dists.append(np.inf)
                continue
            #print new_points[-1], test
            old_vec = new_points[-1] - center
            new_vec = test - center
            diff = old_vec - new_vec
            angle = np.abs(np.arctan2(diff[1], diff[0]))
            #angle = np.abs(np.dot(old_vec, new_vec) / (np.linalg.norm(old_vec) * np.linalg.norm(new_vec)))
            dists.append(angle)
        chosen = np.argmin(dists)+1
        new_order.append(chosen)
        new_points.append(points[chosen])
        used[chosen-1] = True
    new_points.append(points[-1])
    new_order.append(len(points)-1)
    print new_order
    #print used
    return np.array(new_points), np.array(new_order)


def get_random_circle_points(center, r, num):
    rpoints = []
    chosen_angles = []
    min_angle = 2.0 * np.pi / (num*5.0)
    for n in xrange(num):
        tries = 5
        angle = np.random.rand() * 2 * np.pi
        while (len(chosen_angles) > 0 and np.amin(np.abs(np.array(chosen_angles) - angle)) < min_angle):
            angle = np.random.rand() * 2 * np.pi
            tries -= 1
            if tries == 0:
                break
        dx, dy = pol2cart(r, angle)
        rpoints.append([dx, dy])
        chosen_angles.append(angle)
    rpoints = np.array(rpoints)
    return rpoints + center


def find_circle_points(me, apprx_dist, points, weights, text_in):
    text = np.array(text_in)
    r = apprx_dist / (2.0*np.pi)
    angle = np.random.rand() * 2 * np.pi
    dx, dy = pol2cart(r, angle)
    center = me + np.array([dx, dy])

    close = find_close_to_circle_weighted(center, r, points, weights)
    cur_rand_points = num_rand_points
    if len(close) + num_rand_points < min_points:
        cur_rand_points = min_points - len(close)
    rpoints = get_random_circle_points(center, r, cur_rand_points)
    text = ["Start"] + list(np.array(text)[close]) + [""] * len(rpoints) + ["End"]
    print len(text)
    chosen_points = np.vstack([[me], points[close], rpoints, [me]])
    chosen_points, new_order = order_points_tsp(chosen_points)
    text = np.array(text)[new_order]
    print new_order
    #route_points(points)
    return chosen_points, text

def get_circle(lat, lng, path_length):
    #ll, w, text = load_points()
    ll, w, text = get_nearby_features(lat, lng, path_length)
    print ll.shape[0]
    if len(ll) == 0:
        ll = np.array([[lat, lng]])
        w = np.array([1.0])
        text = [""]

    points = project_points(ll)
    me = np.array([lat, lng])
    me = project_points([me])[0]
    cp, text = find_circle_points(me, path_length, points, w, text)
    pcp = project_points(cp, inverse=True)
    return pcp.tolist(), text.tolist()

def main():
    me = np.array([-35.2779, 149.1135])
    pcp,text = get_circle(me[0], me[1], 5000)
    gpx = gpxpy.gpx.GPX()
    gpx_track = gpxpy.gpx.GPXTrack()
    gpx.tracks.append(gpx_track)
    gpx_seg = gpxpy.gpx.GPXTrackSegment()
    gpx_track.segments.append(gpx_seg)
    for p in pcp:
        gpx_seg.points.append(gpxpy.gpx.GPXTrackPoint(p[0], p[1]))
    fout = open("gen_gpx.gpx", "w") 
    fout.write(gpx.to_xml())
    fout.close()
    #chosen_points = filter_points(me, points, w)
    #route = route_points(me, points[chosen_points])

if __name__ == "__main__":
    main()
