/*global google*/

export function setMapCenter(map) {
  if (navigator.geolocation) {
    return navigator.geolocation.getCurrentPosition(function(location) {
      map.setCenter(new google.maps.LatLng(location.coords.latitude, location.coords.longitude));
    });
  } else {
    var geocoder = new google.maps.Geocoder();
    var address = "Canberra";
    return geocoder.geocode( { 'address': address}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        return new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
      } else {
        return new google.maps.LatLng(-35.280937, 149.130009);
      }
    });
  }
}
