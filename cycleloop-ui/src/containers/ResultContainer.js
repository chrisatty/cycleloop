/*global google*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Map from '../components/Map';
import Instructions from '../components/Instructions';
import ResultInfo from '../components/ResultInfo';
import { Tabs, Tab } from 'react-bootstrap';

class ResultContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      directions: null,
      distance: null,
      duration: null,
      markers: null,
      directionsService: new google.maps.DirectionsService(),
    };
  }

  createRoute(routeData) {
    const routeCoords = routeData.path;
    if (routeCoords === null || typeof routeCoords === "undefined") {
      this.setState({
        directions : null,
        markers: null,
        distance: null,
        duration: null,
      });
      return;
    }
    const routeArray = [];
    const markerArray = [];
    for (let j=0; j< routeCoords.length; j ++) {
      routeArray[j] = new google.maps.LatLng(
        routeCoords[j][0], routeCoords[j][1]
      );
      markerArray[j] = {
        latlng: new google.maps.LatLng(
          routeCoords[j][0], routeCoords[j][1]
        ),
        text: routeData.text[j],
      }
    }

    var request = {
      origin: routeArray[0],
      destination: routeArray[routeArray.length - 1],
      travelMode: 'BICYCLING',
      optimizeWaypoints: true,
    };
    const waypoints = [];
    for (var i = 1; i < routeArray.length - 1; i++) {
      waypoints[i - 1] = {location: routeArray[i], stopover: false};
    }
    request.waypoints = waypoints;
    var self = this;
    this.state.directionsService.route(request, function(directions, status) {
      if (status === 'OK') {
        var distance = 0;
        var duration = 0;
        for(var i = 0; i < directions.routes.length; i++) {
            var route = directions.routes[i];
            for(var j = 0; j < route.legs.length; j++) {
                distance += route.legs[j].distance.value;
                duration += route.legs[j].duration.value;
            }
        }
        self.setState({
          directions,
          distance,
          duration,
          markers:markerArray
        });
      } else {
        alert("Sorry, no routes found. Remember, this only works in Canberra");
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.route !== null && typeof nextProps.route !== "undefined") {
      this.createRoute(nextProps.route);
    }
  }

  render() {
    let instructions = [];
    let instructionsComponent = [];
    if (this.state.directions !== null) {
      const route = this.state.directions.routes[0];
      for(let i = 0; i<route.legs.length; i++) {
        for(let j = 0; j<route.legs[i].steps.length; j++) {
          instructions.push(route.legs[i].steps[j]);
        }
      }
    }
    if (instructions.length > 0) {
      instructionsComponent = <Instructions instructions={instructions} />
    }

    return (
      <div id="tab-container">
        <Tabs defaultActiveKey={1} id="map-data-tabs">
          <Tab eventKey={1} title="Map">
            <Map markers={this.state.markers} directions={this.state.directions} />
            <ResultInfo distance={this.state.distance} duration={this.state.duration} />
            {instructionsComponent}
          </Tab>
          <Tab eventKey={2} title="Data">
            <div id="elasticsearch">
              <h2>Explore our data in Kibana</h2>
              <p><a target="_blank" href="http://govhack.co:5602/app/kibana#/dashboard/e191a9f0-74b2-11e7-91d1-afa806badf47?embed=true&_g=()">All Points of Interest</a></p>
              <p><a target="_blank" href="http://govhack.co:5602/app/kibana#/dashboard/babc61a0-74b5-11e7-91d1-afa806badf47?embed=true&_g=()">Fitness Locations</a></p>
              <p><a target="_blank" href="http://govhack.co:5602/app/kibana#/dashboard/b847f020-74be-11e7-91d1-afa806badf47?embed=true&_g=()">Public Art Locations</a></p>
              <p><a target="_blank"  href="http://govhack.co:5602/app/kibana#/dashboard/b4bca580-74c9-11e7-91d1-afa806badf47?embed=true&_g=()">Public Toilet Locations</a></p>
            </div>
          </Tab>
        </Tabs>
      </div>
    )
  }

}

ResultContainer.props = {
  route: PropTypes.array,
}

export default ResultContainer;
