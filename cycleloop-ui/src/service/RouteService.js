/*global google*/

export function getRoute(startLocation, length) {
  let prefix = "";
  if (window.location.hostname.indexOf("govhack") !== -1) {
    prefix = "http://govhack.co:5000/";
  }


  return fetch(prefix + "getpath?lat=" + startLocation.lat() + "&lng="+ startLocation.lng() + "&plen=" + ((length*1000)/2), {
    headers: {
      'Accept': 'application/json',
    },
    method: "GET"
  })
  .then(function(res){
    return res.json();
  }).then(function(json){
    return json;
  })
  .catch(function(res){
    console.log(res);
    alert("Could not connect to server, please try again later")
  });
  //return localRoute(startLocation, length);
}

function toRadians(degrees) {
  return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
function toDegrees(radians) {
  return radians * 180 / Math.PI;
};

function calculateMidPoint(lat1, lon1, lat2, lon2){
    var dLon = toRadians(lon2 - lon1);
    //convert to radians
    lat1 = toRadians(lat1);
    lat2 = toRadians(lat2);
    lon1 = toRadians(lon1);
    var Bx = Math.cos(lat2) * Math.cos(dLon);
    var By = Math.cos(lat2) * Math.sin(dLon);
    var lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
    var lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
    //print out in degrees
    return new google.maps.LatLng(toDegrees(lat3), toDegrees(lon3));
}

function localRoute(startLocation, length) {
  var approxLength = length * 0.9;
  var diameter = approxLength / Math.PI;
  var radius = diameter / 2;
  const routes = [];

  var randomDegrees = Math.random() * 360;
  var start = startLocation;
  routes[routes.length] = start;

  var midPoint = google.maps.geometry.spherical.computeOffset(start, (radius * 1000), randomDegrees);

  var centreOfCircle = calculateMidPoint(start.lat(), start.lng(), midPoint.lat(), midPoint.lng());
  //routes[routes.length] = centreOfCircle;

  for (var i = -150; i < 0; i += 30) {
       var point = google.maps.geometry.spherical.computeOffset(centreOfCircle, (radius/2 * 1000), randomDegrees + i);
       routes[routes.length] = point;
  }
  routes[routes.length] = midPoint;

  for (var j = 30; j < 180; j += 30) {
       var point = google.maps.geometry.spherical.computeOffset(centreOfCircle, (radius/2 * 1000), randomDegrees + j);
       routes[routes.length] = point;
  }
  return routes;
}
