import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ResultInfo extends Component {

  getHours(seconds) {
    return Math.floor(seconds / 3600);
  }

  getMinutes(seconds) {
    return Math.floor(seconds / 60) % 60;
  }

  getKm(distance) {
    return Math.round(distance / 1000, 2);
  }

  render() {
    return (
      <div className="resultInfo">
        {this.props.distance !== null &&
        <p>
          <b>Total distance</b>: {this.getKm(this.props.distance)} km
        </p>
        }
        {this.props.duration !== null &&
        <p>
          <b>Total Duration</b>: {this.getHours(this.props.duration)} hours, {this.getMinutes(this.props.duration)} minutes
        </p>
        }
      </div>
    )
  }

}

ResultInfo.props = {
  distance: PropTypes.number,
  duration: PropTypes.number,
}

export default ResultInfo;
