/*global google*/
import React, { Component } from 'react';
import { FormGroup, FormControl, Button, Form, Col, InputGroup, MenuItem, DropdownButton } from 'react-bootstrap';
import PropTypes from 'prop-types';

class RouteForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchBox: null,
      showErrors: false,
    };
  }

  componentDidMount() {
    var input = document.getElementById('location-input');
    var searchBox = new google.maps.places.SearchBox(input);
    this.setState({searchBox});
  }

  letsGo() {
    const location = document.getElementById("location-input").value;
    const distance = document.getElementById("distance-input").value;
    if (location.length === 0 || distance.length === 0) {
      alert("Please fill in all fields");
      this.setState({showErrors: true});
    } else {
      var geocoder = new google.maps.Geocoder();
      const self = this;
      geocoder.geocode( { 'address': location}, function(results, status) {
        if (status === 'OK') {
          self.props.onSubmit(results[0].geometry.location, distance);
        } else {
          alert("Sorry a problem occured, please try again");
        }
      });
    }
  }

  updateIfRequired(element) {
    if (this.state.showErrors && document.getElementById(element) && document.getElementById(element).value.length <= 1) {
      this.forceUpdate();
    }
  }

  updateLocation(val) {
    document.getElementById("distance-input").value = val;
    this.forceUpdate();
  }

  render() {
    let distanceValidation = null;
    if (document.getElementById("distance-input") && document.getElementById("distance-input").value.length === 0 && this.state.showErrors) {
      distanceValidation = "error";
    }

    let locationValidation = null;
    if (document.getElementById("location-input") && document.getElementById("location-input").value.length === 0 && this.state.showErrors) {
      locationValidation = "error";
    }

    return (
      <Form inline>
        <FormGroup validationState={locationValidation}>
          <Col sm={10}>
            <input id="location-input" type="text" className="form-control" placeholder="Enter Start Location"  onChange={() => this.updateIfRequired("location-input")} />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup validationState={distanceValidation}>
          <Col sm={10}>
            <InputGroup>
              <FormControl id="distance-input" type="text" placeholder="Distance in KM" onChange={() => this.updateIfRequired("distance-input")}/>
              <DropdownButton
                componentClass={InputGroup.Button}
                id="input-dropdown-addon"
                title=""
                onSelect={(val) => this.updateLocation(val)}
                >
                <MenuItem eventKey="10">10KM</MenuItem>
                <MenuItem eventKey="15">15KM</MenuItem>
                <MenuItem eventKey="20">20KM</MenuItem>
                <MenuItem eventKey="25">25KM</MenuItem>
                <MenuItem eventKey="30">30KM</MenuItem>
                <MenuItem eventKey="40">40KM</MenuItem>
                <MenuItem eventKey="50">50KM</MenuItem>
              </DropdownButton>
              <FormControl.Feedback />
            </InputGroup>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col sm={10}>
            <Button bsStyle="primary" bsSize="large" onClick={this.letsGo.bind(this)}>Go</Button>
          </Col>
        </FormGroup>
      </Form>
    )
  }
}

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

export default RouteForm;
