
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';

class Instructions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    }
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }

  getInstructionHtml(instruction) {
    let html = instruction.instructions;
    if (instruction.distance.value > 200) {
      html += " for " + instruction.distance.text;
    }
    return html;
  }

  render() {
    return (
      <div>
        <p className="directions-click" onClick={this.open.bind(this)}>Click here for detailed directions</p>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Route Directions</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <ol>
            {this.props.instructions.map((instruction, key) =>
              <li key={key} dangerouslySetInnerHTML={{ __html: this.getInstructionHtml(instruction) }}></li>
            )}
            </ol>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

}

Instructions.propTypes = {
  instructions: PropTypes.array.isRequired,
}

export default Instructions;
