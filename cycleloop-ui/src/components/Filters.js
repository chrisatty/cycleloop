
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormGroup, Checkbox } from 'react-bootstrap';

class Filters extends Component {

  renderFilters(filters) {
    const renderFilters = [];

    for (var key in filters) {
      const name = key;
      renderFilters.push(
        <FormGroup>
          <Checkbox key={key} checked={filters[key]} onChange={(event) => this.filterChange(name, event.target.checked)}>
            {key}
          </Checkbox>
        </FormGroup>
      )
    }

    return renderFilters;
  }

  filterChange(filterName, checked) {
    this.props.onFilterChange(filterName, checked);
  }

  render() {

    const filters = this.renderFilters(this.props.filters);
    if (filters.length === 0) {
        return <div></div>;
    }

    return (
      <div className="filters">
        <p><b>Show:</b></p>
        <ul>
          {filters}
        </ul>
      </div>
    )
  }

}

Filters.propTypes = {
  filters: PropTypes.array,
  onFilterChange: PropTypes.func.isRequired,
}

export default Filters;
