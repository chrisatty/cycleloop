/*global google*/
import React, { Component } from 'react';
import * as LocationUtils from '../utils/LocationUtils';
import '../styles/map.css';
import PropTypes from 'prop-types';
import Filters from './Filters';

class Map extends Component {

  constructor(props) {
    super(props);
    this.state = {
      directionsDisplay: new google.maps.DirectionsRenderer(),
      map: null,
      mapMarkers: [],
      filters: [],
    };
  }

  populateMap() {
    if (this.props.directions !== null && "undefined" !== typeof this.props.directions) {
      this.state.directionsDisplay.setDirections(this.props.directions);
      if (this.props.markers !== null && "undefined" !== typeof this.props.markers) {
        for (let i =0; i < this.props.markers.length; i++) {
          if (this.props.markers[i].text.trim().length > 0) {
            const infowindow = new google.maps.InfoWindow({
              content: this.props.markers[i].text
            });

            const marker = new google.maps.Marker({
              position: this.props.markers[i].latlng,
              title: this.props.markers[i].text,
            });
            this.state.mapMarkers.push(marker);
            const self = this;
            marker.addListener('click', function() {
              infowindow.open(self.state.map, marker);
            });
            if (this.props.markers[i].text.trim() === "Toilet") {
              marker.setIcon('toilet-icon.png');
            } else if(this.props.markers[i].text.trim() === "Drinking Fountain") {
              marker.setIcon('water-icon.png');
            } else if (this.props.markers[i].text.trim() === "Public Furniture") {
              marker.setIcon('picnic-icon.png');
            } else if (this.props.markers[i].text.trim() === "Barbecue") {
              marker.setIcon('bbq-icon.png');
            } else if (this.props.markers[i].text.trim() === "Public Art") {
              marker.setIcon('art-icon.png');
            } else if (this.props.markers[i].text.trim() === "Graffiti Site") {
              marker.setIcon('graffiti.png');
            } else if (this.props.markers[i].text.trim() === "Fitness Site") {
              marker.setIcon('fitness.png');
            }
          }
        }
      }
    }
    this.renderMarkers();
  }

  renderMarkers() {
    for (let i = 0; i < this.state.mapMarkers.length; i++) {
      if (this.state.filters[this.state.mapMarkers[i].title]) {
        this.state.mapMarkers[i].setMap(this.state.map);
      } else {
        this.state.mapMarkers[i].setMap(null);
      }
    }
  }


  componentWillReceiveProps(nextProps) {
    for (let i = 0; i < this.state.mapMarkers.length; i++) {
      this.state.mapMarkers[i].setMap(null);
    }
    this.setState(
      {mapMarkers: []}
    );

    const filters = [];
    if (nextProps.markers !== null && "undefined" !== typeof nextProps.markers) {
      for(let j = 0; j < nextProps.markers.length; j++) {
        if (nextProps.markers[j].text.trim().length > 0) {
          filters[nextProps.markers[j].text] = true;
        }
      }
    }
    this.setState({filters});
  }

  componentDidUpdate() {
    this.populateMap();
  }

  componentDidMount() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(-35.280937, 149.130009),
      zoom: 12
    });
    this.setState({map});
    LocationUtils.setMapCenter(map);
    this.state.directionsDisplay.setMap(map);

    this.populateMap();
  }

  onFilterChange(filterName, active) {
    const newFilters = this.state.filters;
    newFilters[filterName] = active;
    this.setState({filters: newFilters});
  }

  render() {
    return (
      <div>
        <div id="map"></div>
        <Filters filters={this.state.filters} onFilterChange={this.onFilterChange.bind(this)}/>
      </div>
    )
  }
}

Map.propTypes = {
  directions: PropTypes.object,
  markers: PropTypes.array,
}

export default Map;
