import React, { Component } from 'react';
import './App.css';
import ResultContainer from './containers/ResultContainer';
import RouteForm from './components/RouteForm';
import * as RouteService from './service/RouteService';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      routeData: null,
    }
  }

  calculateRoute(location, distance) {
    const self = this;
    RouteService.getRoute(location, distance).then(function(routeData) {
      self.setState({routeData});
    });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src="logo.png" className="logo" alt="Scenic Cycle"/>
          <h2>Welcome to Scenic Cycle Canberra</h2>
        </div>
        <div className="App-intro">
          <p>Want to go for a cycle and don&#39;t know where to go?</p>
          <p>Create your journey now! Enter your starting point in Canberra and how far you want to travel.</p>
        </div>
        <RouteForm onSubmit={this.calculateRoute.bind(this)}/>
        <ResultContainer route={this.state.routeData} />
      </div>
    );
  }
}

export default App;
